import { useState } from 'react';
import './App.css';
import { BrowserRouter , Route, Routes} from 'react-router-dom';
import Form from './components/Form';
import List from './components/List';
import Navbar from './components/Navbar';
import Register from './components/Register';
import Signin from './components/Signin';
import Arraycp from './components/Arraycp';

export interface IState {
  infor: {
    name: string,
    age: number,
    email: string
  }[]
}
export interface IInvoiceListProps {
  invoiceData: {
    customerFirstName: string,
    customerLastName: string,
    invoices: {
      id: number,
      name: string,
      total: number,
      paymentStatus: string
    }[]
  }
}

function App() {
  const [infor, setInfor] = useState<IState['infor']>([])
  const data1 = {
    customerFirstName: 'Văn A',
    customerLastName: 'Trần',
    invoices: [
      { id: 100, name: 'Invoice 001', total: 20000, paymentStatus: 'paid'},
      { id: 101, name: 'Invoice 002', total: 50000, paymentStatus: 'pending'},
      { id: 102, name: 'Invoice 003', total: 50000, paymentStatus: 'pending'},
      { id: 103, name: 'Invoice 004', total: 20000, paymentStatus: 'pending'},
      { id: 104, name: 'Invoice 005', total: 30000, paymentStatus: 'late'},
      { id: 105, name: 'Invoice 006', total: 20000, paymentStatus: 'late'},
    ],
  };
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar/>
        <Routes>
          <Route path='/' element={<Form infor={infor} setInfor={setInfor}/>}></Route>
          <Route path='/submited' element={<List infor={infor}/>}></Route>
          <Route path='/arraycp' element={<Arraycp invoiceData={data1}/>}></Route>
          <Route path='/register' element={<Register/>}></Route>
          <Route path='/signin' element={<Signin/>}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
