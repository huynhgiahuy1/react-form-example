import { IInvoiceListProps as IProps } from '../App'
import '../css/Arraycp.css'

export default function Arraycp(props: IProps) {
  const {customerFirstName, customerLastName, invoices} = props.invoiceData;
  const itemStyle = {
      display: 'flex',
      justifyContent: 'space-between'
  }

  const matchsTest1 = invoices.filter(invoice => invoice.total >= 30000);
  const matchsTest2 = invoices.filter(invoice => invoice.paymentStatus === 'late')
  const matchsTest3 = invoices.sort((invoice1, invoice2) => (invoice1.total > invoice2.total) ? 1 : (invoice1.total < invoice2.total) ? -1 : 0);

  return (
    <div>
        <h1 className='invoice-customer-name'>{customerLastName} {customerFirstName}</h1>
        <hr/>
        <div>
            {invoices.map((invoice) => (
                <ul key={invoice.id} style={itemStyle}>
                    <li>{invoice.name}</li>
                    <div className='invoice-total'>{invoice.total} ({invoice.paymentStatus})</div>
                </ul>
            ))}
             
        </div>

        <h1 className="testing-function">Testing Function</h1>
        <hr/>
        <div>
            <div className="invoice-name-testing">*Invoice ID that greater than 30000: </div>
            {matchsTest1.map(match => (
            <ul key={match.id}>
                <li>{match.name}</li>
            </ul>
            ))}
        </div>

        <div>
            <div className='invoice-name-testing'>*Invoice ID that is late: </div>
            {matchsTest2.map(match => (
                <ul key={match.id}>
                    <li>{match.name}</li>
                </ul>
            ))}
        </div>
         
        <div>
            <div className='invoice-name-testing'>*Sorted Invoice ascending: </div>
            {matchsTest3.map(match => (
                <ul key={match.id}>
                    <li>{match.name}</li>
                </ul>
            ))}
        </div>

    </div>
  )
}
