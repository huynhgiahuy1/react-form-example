import { Link } from 'react-router-dom'
import '../css/Navbar.css'

function Navbar() {
    return (
        <div className="navbar">
            <ul className="navbar-list">
                <li className="navbar-item">
                    <Link to='/' className='navbar-items'>Info</Link>
                </li>
                <li className="navbar-item">
                    <Link to='/submited' className='navbar-items'>Submited Info</Link>
                </li>
                <li className="navbar-item">
                    <Link to='/arraycp' className='navbar-items'>Array Testing</Link>
                </li>
                <li className="navbar-item">
                    <Link to='/register' className='navbar-items'>Register</Link>
                </li>
                <li className="navbar-item">
                    <Link to='/signin' className='navbar-items'>Sign In</Link>
                </li>
            </ul>
        </div>
    )
}

export default Navbar