import React, { useState } from 'react'
import '../css/Form.css'
import {IState as Props} from '../App'

interface IProps {
  infor: Props['infor'],
  setInfor: React.Dispatch<React.SetStateAction<Props['infor']>>

}


export default function Form({infor, setInfor}: IProps) {
  const [input, setInput] = useState({name: "", age: "", email: ""})
  
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInput({ ...input,[e.target.name] : e.target.value })
  }

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setInfor([...infor, {
      name: input.name,
      age: Number(input.age),
      email: input.email

    }])
    setInput({name: "", age: "", email: ""})
  }

  return (
    <div className='form-container'>
        <h1>Testing Props and State</h1>
        <form onSubmit={onSubmit}>
            <input type='text' name = 'name' id = 'name' placeholder='Enter name' onChange={onChange} value ={input.name} required/>
            <input type='number' name = 'age' id ='age' placeholder='Enter age' onChange={onChange} value ={input.age} required/>
            <input type='email' name = 'email' id = 'email' placeholder='Enter email' onChange={onChange} value ={input.email} required/>
            <button type='submit'>Submit</button>
        </form>
    </div>
  )
}
