import '../css/List.css'
import {IState as IProps} from '../App'

export default function List({infor}: IProps) {
  const renderList = () => {
      return (
      infor.map(peopleInfo => {
          return (
            <div className='list-item' key={peopleInfo.name}>
                {peopleInfo.name} - {peopleInfo.age} - {peopleInfo.email}
            </div>
          )
      })
      )
  }  
  return (
    <div className='list-container'>
        <h1>Submited List</h1>
        {renderList()}
    </div>
  )
}
